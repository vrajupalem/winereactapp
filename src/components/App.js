import React, {Component} from 'react'
import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'
import WineData from '../data/11YVCHAR001.json'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wineComps: [],
            wineDetails: [],
            columnsWineComps : [{
              Header: 'Percentage',
              accessor: 'percentage',
              style: {
                  textAlign: "center"
              },
              width: 100,
              maxWidth: 100,
              minWidth: 100
            }, {
              Header: 'Year',
              accessor: 'year',
              sortable: false,
              filterable: false,
              style: {
                  textAlign: "center"
              },
              width: 100,
              maxWidth: 100,
              minWidth: 100
            }, {
              
              Header: 'Variety',
              accessor: 'variety',
              sortable: false,
              filterable: false
            }, {
              Header: 'Region',
              accessor: 'region',
              sortable: false
            }]
  
        }
    }
    
    componentDidMount () {
        this.setState({wineComps: WineData.components});
        const wineData = [{
            lotCode : WineData.lotCode,
            volume : WineData.volume,
            description : WineData.description,
            tankCode : WineData.tankCode,
            productState : WineData.productState,
            ownerName : WineData.ownerName
        }];

        this.setState({wineDetails: wineData});
    }

    toggleColumn = n => {
      const cols = this.state.columnsWineComps.map((col, i) => n===i? {...col, show: !col.show}: col)
      this.setState({
        columnsWineComps: cols
      })
    }
    render() {

          const columnsWineDetails = [{
            Header: 'Lot Code',
            accessor: 'lotCode',
            sortable: false,
            filterable: false,
            width: 200,
            maxWidth: 200,
            minWidth: 200 
          }, {
            Header: 'Volume',
            accessor: 'volume',
            sortable: false,
            filterable: false,
            width: 100,
            maxWidth: 100,
            minWidth: 100
          }, {
            
            Header: 'Description',
            accessor: 'description',
            sortable: false,
            filterable: false,
          }, {
            Header: 'Tank Code',
            accessor: 'tankCode',
            sortable: false,
            filterable: false,
            width: 100,
            maxWidth: 100,
            minWidth: 100
          }, {
            Header: 'Product State',
            accessor: 'productState',
            sortable: false,
            filterable: false,
            width: 200,
            maxWidth: 200,
            minWidth: 200
          }, {
            Header: 'Owner Name',
            accessor: 'ownerName',
            sortable: false,
            filterable: false,
          }]

          return <div id="app-container">
                    <h1> Wine Details </h1>
                    <ReactTable
                      data={this.state.wineDetails}
                      columns={columnsWineDetails}
                      defaultPageSize={1}
                      showPagination = {false}
                    />  

                    <h1> Wine Components </h1>
                    <ReactTable
                      data={this.state.wineComps}
                      columns={this.state.columnsWineComps}
                      sorted={[
                        {
                        id: 'percentage',
                        desc: true
                        }
                      ]}
                      filterable
                      defaultPageSize={5}
                    />
                    <div>
                      <button style={{backgroundColor: "green", color: "#fefefe"}} onClick={() => this.toggleColumn(1)}>Toggle Year</button>
                      <button style={{backgroundColor: "blue", color: "#fefefe"}} onClick={() => this.toggleColumn(2)}>Toggle Variety</button>
                      <button style={{backgroundColor: "orange", color: "#fefefe"}} onClick={() => this.toggleColumn(3)}>Toggle Region</button>
                    </div>
               
                </div>
    }
}